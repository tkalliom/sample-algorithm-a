FROM nvidia/cuda:8.0-cudnn5-runtime

WORKDIR /app/

RUN apt-get update \
  && apt-get install -y software-properties-common && add-apt-repository ppa:timsc/opencv-3.2 \
  && apt-get update && apt-get -y install --no-install-recommends python3-opencv python3-numpy nodejs

COPY . .

ENV PORT 80
EXPOSE 80

CMD ["nodejs", "./algorithm-server/app.js"]
