const algorithm = {
    name: "sample-algorithm-a",
    version: 1,
    port: process.env.PORT || 3001
};

const program = {
    command: "python3", // interpreter or binary
    args: ["detection.py"] //may be left empty
}

const config = {
    algorithm: algorithm,
    program: program
}

module.exports = config;
